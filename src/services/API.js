import axios from "axios";

const apiInstance = axios.create({
  responseType: "json",
});

export default apiInstance;

import React from "react";
import "./App.css";
import { Helmet } from "react-helmet";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import { hot } from "react-hot-loader";
import { Container, CssBaseline } from "@material-ui/core";

function App() {
  const history = createBrowserHistory();

  return (
    <>
      <Helmet defaultTitle="WB Junior Frontend Interview" />
      <Router history={history}>your code</Router>
    </>
  );
}

export default hot(module)(App);
